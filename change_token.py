#!/usr/bin/env python

from libs.initialize import *
from libs.gamebox import *
import random, string
import hashlib
from multiprocessing.pool import ThreadPool as Pool
import logging
import sys
from manager import api, host

pwn_service = []
web_service = []
logger = logging.getLogger('CTF logger - change token')

def init_logger():
    global logger
    logger.setLevel(logging.DEBUG)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(logging.Formatter('%(asctime)s %(message)s', datefmt='%Y/%m/%d %H:%M:%S'))
    logger.addHandler(ch)

    dir_path = os.path.dirname(os.path.abspath(__file__))
    ch = logging.FileHandler(dir_path + '/change_token.log')
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(logging.Formatter('%(asctime)s %(message)s', datefmt='%Y/%m/%d %H:%M:%S'))
    logger.addHandler(ch)

def gen_token():
    length = 128
    randstr = ''.join([random.choice(string.digits + string.lowercase + string.uppercase) for i in xrange(length)])

    flag = hashlib.md5(randstr).hexdigest()

    return flag

def init_service():
    global pwn_service

    with open(CONFIG_ETC_PATH + '/service_pwn.lst', 'r') as fh:
        for service in fh:
            pwn_service.append(service.strip())

    with open(CONFIG_ETC_PATH + '/service_web.lst', 'r') as fh:
        for service in fh:
            web_service.append(service.strip())

def update_web(ip, service, token):
    #echo [+] `date` Update token Status: `timeout 4 curl -s http://10.10.10.1/admin/update_flag -d "ip=$ip&services[$service]=$token"` - $ip $service
    data = {
            'ip': ip,
            'services[%s]' % (service): token
            }
    m = api('post', 'http://%s/admin/update_flag' % (host), data = data)
    return m

current_round = -1

def get_current_round():
    global current_round

    with open(CONFIG_ETC_PATH + '/current_round', 'r') as fh:
        current_round = int(fh.read().strip())

def update_round(r):
    with open(CONFIG_ETC_PATH + '/current_round', 'w') as fh:
        fh.write(str(r) + '\n')

def change_token_thread(ip, service):
    global current_round
    token = gen_token()
    gb = Gamebox(ip)
    status = ''
    try:
        # update to gamebox
        gb.change_token(service, token)
        # update to web
        m = update_web(ip, service, token)
        status = 'ok - %s' % (m)
    except Exception as e:
        status = 'fail - %s' % (str(e))

    msg = 'Round: %d [%s]: [%s] change token to [%s] %s' % (current_round, ip, service, token, status)

    global logger
    logger.debug(msg)

if __name__ == '__main__':
    init_logger()

    get_current_round()
    if current_round == -1:
        logger.debug('Please initialize CTF game first')
        sys.exit()

    init_service()
    if len(pwn_service) == 0 and len(web_service) == 0:
        logger.debug('Please deploy at least 1 service for the game')
        sys.exit()

    # setup next round
    m = api('get', 'http://%s/admin/set_round' % host)
    logger.debug(m)

    current_round += 1

    pool = Pool(8)

    for service in pwn_service:
        for ip in PWN_GB_IP_LIST:
            pool.apply_async(change_token_thread, (ip, service))
    for service in web_service:
        for ip in WEB_GB_IP_LIST:
            pool.apply_async(change_token_thread, (ip, service))

    pool.close()
    pool.join()

    # update round
    update_round(current_round)
