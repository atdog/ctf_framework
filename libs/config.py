#!/usr/bin/env python

import os

DEBUG = False

CONFIG_FILE_PATH = os.path.abspath(__file__)

CONFIG_DIR_PATH = os.path.dirname(CONFIG_FILE_PATH) + "/"

CONFIG_ETC_PATH = os.path.abspath(CONFIG_DIR_PATH + "../etc/")

CONFIG_PWN_GB_TARGET_LIST = os.path.abspath(CONFIG_ETC_PATH + "/pwn_gb.lst")
CONFIG_WEB_GB_TARGET_LIST = os.path.abspath(CONFIG_ETC_PATH + "/web_gb.lst")

CONFIG_KEY_DIR_PATH = os.path.abspath(CONFIG_ETC_PATH + "/cred")

CONFIG_SERVICE_DIR_PATH = os.path.abspath(CONFIG_ETC_PATH + "/services")
