#!/usr/bin/env python

from config import *
from log import *
import subprocess
import shlex
import re
import glob
import random, string
import hashlib
import os

class Gamebox:

    def __init__(self, ip):
        self.ip = ip
        self.key = CONFIG_KEY_DIR_PATH + "/gamebox/root.key"

        debug("Init Gamebox: %s" % (self.ip))
        debug("Init Key %s" % (self.key))

        self.ping_pattern = re.compile(r'.*bytes from.*ttl=\d+ time=([\d\.]+) ms', re.DOTALL)

    def execute_on_gamebox(self, cmd, timeout = 3):
        host = "root@%s" % (self.ip)
        # setup ssh for only 5 seconds.
        cmd = ["ssh", "-o", "StrictHostKeyChecking=no", "-o", "ConnectTimeout=5", "-i", self.key, host, "bash", "-c", "'", cmd, "'"]
        output = self.execute(cmd)
        debug("result on %s: %s" % (self.ip, output))
        return output

    def execute(self, cmd):
        try:
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            debug("execute %s" % (" ".join(cmd)))
            p.wait()
            if p.returncode != 0:
                raise Exception('shell', 'fail: %s', ' '.join(cmd))
            return p.stdout.read()
        except subprocess.CalledProcessError as e:
            debug(e.cmd)
            debug(e.returncode)
            debug(e.output)
            raise

    def copy_from_local(self, src, dst):
        cmd = "scp -o StrictHostKeyChecking=no -o ConnectTimeout=5 -i %s -r %s root@%s:%s" % (self.key, src, self.ip, dst)
        cmd = shlex.split(cmd)

        output = self.execute(cmd)

    def init_setup(self):
        dst_path = "/root/"
        glob_path = CONFIG_ETC_PATH + "/setup/*"
        for src_path in glob.glob(glob_path):
            # scp file to gamebox
            self.copy_from_local(src_path, dst_path)

        # execute setup.sh
        self.execute_on_gamebox("chmod +x /root/init_setup.sh && /root/init_setup.sh")

    def ping_check(self):
        cmd = "timeout 1s ping -c 1 %s" % (self.ip)
        cmd = shlex.split(cmd)

        output = self.execute(cmd)

        m = self.ping_pattern.match(output)
        if m:
            output = m.group(1)

        return float(output)

    def clean(self):
        self.execute_on_gamebox('/root/clean.sh')
        return 'clean gb: %s success' % (self.ip)

    def deploy_service(self, name):
        # check service existence
        if not os.path.exists(CONFIG_SERVICE_DIR_PATH + "/%s/binary" % (name)):
            return 'service: %s does not exist' % (name)

        # remove original folder
        self.execute_on_gamebox('rm -rf /root/%s' % (name))

        # upload service folder
        dst_path = "/root/"
        src_path = CONFIG_SERVICE_DIR_PATH + "/%s" % (name)

        # scp file to gamebox
        self.copy_from_local(src_path, dst_path)

        # init service
        self.execute_on_gamebox('/root/deploy_service.sh %s' % (name))

        return 'service: %s init success' % (name)

    def start_service(self, name):

        self.execute_on_gamebox('/root/start_service.sh %s' % (name))
        return 'start service: %s success' % (name)

    def change_token(self, service, token):
        cmd = "echo %s > /home/flags/%s" % (token, service)
        self.execute_on_gamebox(cmd)
        return 'change token for service: %s success' % (service)

    def upload_key_to_gb(self, key_path):
        # upload
        self.copy_from_local(key_path, '/root/')
        # setup
        key_name = key_path.split('/')[-1]
        cmd = '''rm -rf /home/ctf/.ssh &&
        mkdir /home/ctf/.ssh &&
        mv /root/%s /home/ctf/.ssh/authorized_keys &&
        chmod 700 /home/ctf/.ssh &&
        chown -R ctf:ctf /home/ctf/.ssh/''' % (key_name)
        self.execute_on_gamebox(cmd)
        return 'upload key success'
