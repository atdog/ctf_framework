#!/usr/bin/env python

from config import *

PWN_GB_IP_LIST = []
WEB_GB_IP_LIST = []

def init_target():
    global PWN_GB_IP_LIST
    global WEB_GB_IP_LIST

    with open(CONFIG_PWN_GB_TARGET_LIST, 'rb') as fh:
        for line in fh:
            gb_ip = line.strip()
            PWN_GB_IP_LIST.append(gb_ip)

    with open(CONFIG_WEB_GB_TARGET_LIST, 'rb') as fh:
        for line in fh:
            gb_ip = line.strip()
            WEB_GB_IP_LIST.append(gb_ip)

init_target()
