#!/usr/bin/env python

from config import *

def debug(msg):
    if DEBUG:
        msg = "[*] " + str(msg)
        print msg
