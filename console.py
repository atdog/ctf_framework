#!/usr/bin/env python
from collections import OrderedDict
from datetime import datetime as dt
import curses
import curses.ascii
import re
import thread
import time
import socket
import manager
import shlex
import sys
import locale
import threading
from libs.config import *
locale.setlocale(locale.LC_ALL, '') 

ex = None
input_win = None
output_win = None
status_win = None
token_win = None
cmd = ''
history = []
history_idx = 0
current_round = -1
teams = None

uilock = threading.Lock()

def setup_status_win():
    global status_win
    (y, x) = status_win.getmaxyx()

    status_win.erase()
    status_win.addstr(1, 1, "Ping status", curses.color_pair(179))
    status_win.hline(2, 1, "-", x-2)
    status_win.box()

    t = dt.strftime(dt.now(), '%Y/%m/%d %H:%M:%S')

    status_win.addstr(1, x - 21, t, curses.color_pair(179))

    status_win.refresh()

def setup_token_win():
    global token_win
    (y, x) = token_win.getmaxyx()

    token_win.erase()
    token_win.addstr(1, 1, "Token Log", curses.color_pair(179))
    token_win.hline(2, 1, "-", x-2)
    token_win.box()

    r = None
    with open(CONFIG_ETC_PATH + '/current_round', 'r') as fh:
        r = fh.read().strip()

    global current_round
    current_round = int(r)

    token_win.addstr(1, x - 15, "Round: %s" % (r), curses.color_pair(179))

    token_win.refresh()

def setup_input_win():
    global input_win
    input_win.erase()
    input_win.box()
    input_win.addstr(1, 1, "Please input your cmd here.", curses.color_pair(244))
    input_win.addstr(2, 1, '$ ')

def setup_output_win():
    global output_win
    (y, x) = output_win.getmaxyx()

    output_win.erase()
    output_win.addstr(1, 1, "HITCON CTF Console", curses.color_pair(179))

    output_win.hline(2, 1, "-", x - 2)
    output_win.box()
    output_win.refresh()

def display_main_layout(y, x):
    global input_win
    global output_win
    global status_win
    global token_win

    input_height = 4
    output_height = y - input_height
    output_width = int(x * 0.60)
    status_width = x - output_width
    status_height = 20
    token_height = output_height - status_height

    output_win = curses.newwin(output_height, output_width, 0, 0)
    setup_output_win()
    output_win.refresh()

    status_win = curses.newwin(status_height, status_width, 0, output_width)
    setup_status_win()
    status_win.refresh()

    token_win = curses.newwin(token_height, status_width, status_height, output_width)
    setup_token_win()
    token_win.refresh()

    input_win = curses.newwin(input_height, x, output_height, 0)
    setup_input_win()

def cursor_left():
    global input_win
    global cmd

    (y,x) = input_win.getyx()
    if (x-1) > 2:
        input_win.move(y, x - 1)

def cursor_right():
    global input_win
    global cmd

    (y,x) = input_win.getyx()
    if (x+1) < (len(cmd) + 4):
        input_win.move(y, x + 1)

def cursor_up():
    global input_win
    global cmd
    global history
    global history_idx

    if -(history_idx) == len(history):
        return

    history_idx -= 1

    setup_input_win()

    # must be set to empty string
    cmd = history[history_idx]
    input_win.addstr(cmd)

def cursor_down():
    global input_win
    global cmd
    global history
    global history_idx

    if -(history_idx) == 1:
        return

    history_idx += 1

    setup_input_win()

    # must be set to empty string
    cmd = history[history_idx]
    input_win.addstr(cmd)

def cursor_bs():
    global input_win
    global cmd

    (max_y, max_x) = input_win.getmaxyx()
    (y,x) = input_win.getyx()

    if (x-1) > 2:
        input_win.delch(y, max_x - 1)
        input_win.delch(y, x-1)
        input_win.box()
        cmd = cmd[0:x-4] + cmd[x-3:]

def cursor_del():
    global input_win
    global cmd

    (max_y, max_x) = input_win.getmaxyx()
    (y,x) = input_win.getyx()

    if (x-2) <= len(cmd):
        input_win.delch(y, max_x - 1)
        input_win.delch(y, x)
        input_win.box()
        cmd = cmd[0:x-3] + cmd[x-2:]

def cursor_del_to_head():
    global input_win
    global cmd

    (max_y, max_x) = input_win.getmaxyx()
    (y,x) = input_win.getyx()

    input_win.delch(y, max_x - 1)
    for _ in xrange(x-3):
        input_win.delch(y, 3)
    input_win.box()
    cmd = cmd[x-3:]

def cursor_move_head():
    global input_win

    input_win.move(2, 3)

def cursor_move_end():
    global input_win
    global cmd

    input_win.move(2, len(cmd) + 3)

def thread_cmd(cmd):
    global output_win
    global uilock
    uilock.acquire()
    try:
        func = getattr(manager, 'my_' + cmd[0])

        args = [output_win]
        if len(cmd) > 1:
            args = args + cmd[1:]

        result = func(args)
        (y, x) = output_win.getyx()
        output_win.addstr(5, 13, " OK", curses.color_pair(120))

        y += 1
        for line in result.split("\n"):
            y += 1
            output_win.addstr(y, 1, line)
    except AttributeError as e:
        (y, x) = output_win.getyx()
        output_win.addstr(y + 2, 1, "Command not found", curses.color_pair(10))
        output_win.addstr(str(e), curses.color_pair(10))
    except Exception as e:
        (y, x) = output_win.getyx()
        output_win.addstr(y + 2, 1, str(e), curses.color_pair(10))

    output_win.refresh()
    uilock.release()

def handler(orig_cmd):
    global output_win

    orig_cmd = re.sub(r' +', ' ', orig_cmd)

    cmd = shlex.split(orig_cmd)

    if len(cmd) == 0:
        return

    setup_output_win()
    output_win.addstr(3, 1, "CMD: ", curses.color_pair(117))
    output_win.addstr(orig_cmd)
    output_win.addstr(5, 1, "RESULT: ... ", curses.color_pair(117))
    output_win.refresh()

    thread.start_new_thread(thread_cmd, (cmd, ))

def cursor_enter():
    global input_win
    global cmd
    global history_idx

    history.append(cmd)

    if len(history) == 100:
        history.pop()

    setup_input_win()

    handler(cmd)

    # must be set to empty string
    cmd = ''
    # must be assigned with 0
    history_idx = 0

def input_loop():
    global input_win
    global cmd
    global uilock
    input_win.keypad(True)

    keys = {
            curses.KEY_LEFT: cursor_left,
            curses.KEY_RIGHT: cursor_right,
            curses.KEY_UP: cursor_up,
            curses.KEY_DOWN: cursor_down,
            curses.KEY_BACKSPACE: cursor_bs,
            curses.KEY_ENTER: cursor_enter,
            curses.ascii.NL: cursor_enter,
            curses.ascii.DEL: cursor_del,
            330: cursor_del,
            21: cursor_del_to_head,
            16: cursor_up,
            14: cursor_down,
            1: cursor_move_head,
            5: cursor_move_end
            }

    input_win.addstr(2, 1, '$ ')
    while True:
        c = input_win.getch()
        # global output_win
        # output_win.addstr(str(c))
        # output_win.refresh()

        uilock.acquire()
        try:
            keys[c]()
        except KeyError:
            (y,x) = input_win.getyx()
            cmd_index = x - 3
            if cmd_index == len(cmd):
                input_win.addch(chr(c))
                cmd += chr(c)
            else:
                input_win.insch(chr(c))
                input_win.move(y, x+1)
                input_win.box()
                cmd = cmd[0:cmd_index] + chr(c) + cmd[cmd_index:]
        uilock.release()

def iptoint(ip):
    return int(socket.inet_aton(ip).encode('hex'),16)

def display_ping_check():
    global status_win
    global teams
    global uilock

    if teams == None:
        with open(CONFIG_ETC_PATH + '/teams.lst', 'r') as fh:
            import json
            teams = json.loads(fh.read().strip())

    manager.ping_check_background()

    team_pattern = re.compile(r'.*\.(?P<id>\d+)\.\d+$', re.DOTALL)

    while True:
        uilock.acquire()
        setup_status_win()

        i = 0

        result = manager.ping_status
        result = OrderedDict(sorted(result.items(), key=lambda t: iptoint(t[0])))

        (max_y, _) = status_win.getmaxyx()
        max_y -= 5

        cur_team = None

        for ip in result.keys():

            x = 1 + (i / max_y) * 25
            y = i % max_y + 3

            ############################
            m = team_pattern.match(ip)
            if m is None:
                print 'fail to parse IP'
                sys.exit()

            if cur_team != m.group('id'):
                cur_team = m.group('id')
                try:
                    status_win.addstr(y, x, "[%2s][%s]" % (cur_team, teams[cur_team].encode('utf-8')), curses.color_pair(244))
                except:
                    status_win.addstr(y, x, "[%2s][Not Found]" % (cur_team), curses.color_pair(244))
                i += 1
            ############################
            x = 1 + (i / max_y) * 25
            y = i % max_y + 3
            s = "%11s: " % (ip)
            status_win.addstr(y, x, s)

            if result[ip] == -1:
                status_win.addstr('fail', curses.color_pair(10))
            else:
                status_win.addstr('%5s ms' % (result[ip]), curses.color_pair(118))

            i += 1

        status_win.refresh()
        uilock.release()

        time.sleep(1)

def tail(filename, linenum=10):
    import subprocess
    cmd = 'tail -n %d %s' % (linenum, filename)
    p = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE)
    p.wait()

    if p.returncode != 0:
        raise Exception('tail', 'fail: return code is not zero')

    return p.stdout.read()

def display_token_log():
    global token_win
    global current_round
    global teams
    global uilock

    if teams == None:
        with open(CONFIG_ETC_PATH + '/teams.lst', 'r') as fh:
            import json
            teams = json.loads(fh.read().strip())

    log_pattern = re.compile(r'.*Round: (?P<round>\d+) \[(?P<ip>\d+\.\d+\.(?P<team>\d+)\.\d+)\]: \[(?P<service>\S+)\] change token to \[(?P<token>\S+)\] (?P<status>[^\-]+) - (?P<api>.*)', re.DOTALL)

    (max_y, _) = token_win.getmaxyx()
    max_y -= 5

    while True:
        uilock.acquire()

        setup_token_win()

        if current_round < 1:
            token_win.addstr(4, 1, 'Run ./change_flag.py whenever you want to start the game.')
        else:
            lines = tail('change_token.log', linenum = 100)
            lines = lines.split("\n")

            logs = {}

            service_len = 10

            for i in range(len(lines) - 1, -1, -1):
                log = lines[i]
                m = log_pattern.match(log)
                if not m:
                    continue

                log_round = int(m.group('round'))
                if log_round == current_round:
                    ip = m.group('ip')
                    team = m.group('team')
                    service = m.group('service')
                    status = m.group('status')
                    api_status = m.group('api')

                    if len(service) > service_len:
                        service = service[0:service_len]

                    if not logs.has_key(team):
                        logs[team] = {}

                    logs[team][service] = {'gb': status, 'api': api_status}

                else:
                    break

            nn = 0

            logs = OrderedDict(sorted(logs.items(), key=lambda t: int(t[0])))
            for key in logs.keys():
                y = nn % max_y + 3
                x = 1 + 25 * (nn / max_y)

                try:
                    token_win.addstr(y, x, "[%2s][%s]" % (key, teams[key].encode('utf-8')), curses.color_pair(244))
                except:
                    token_win.addstr(y, x, "[%2s][Not Found]" % (key), curses.color_pair(244))
                cur_team = key
                nn += 1

                for service in logs[key]:
                    y = nn % max_y + 3
                    x = 1 + 25 * (nn / max_y)
                    token_win.addstr(y, x, "   %s: " % (service))

                    if logs[key][service]['gb'] == 'ok':
                        if logs[key][service]['api'] == 'ok':
                            token_win.addstr("%s" % (logs[key][service]['gb']), curses.color_pair(118))
                        else:
                            token_win.addstr("API", curses.color_pair(177))
                    else:
                        token_win.addstr("%s" % (logs[key][service]['gb']), curses.color_pair(10))
                    nn += 1

        token_win.refresh()
        uilock.release()

        import time
        time.sleep(1)

def signal_handler(signal, name):
    thread.exit_thread()
    thread.exit()
    curses.endwin()
    sys.exit(0)

def main(stdscr):
    import signal
    signal.signal(signal.SIGINT, signal_handler)

    global ex

    try:
        curses.start_color()
        curses.use_default_colors()
        curses.curs_set(1)
        curses.noecho()
        for i in range(0, curses.COLORS):
             curses.init_pair(i + 1, i, -1)
    except Exception, e:
        ex = e

    stdscr.clear()

    (y, x) = stdscr.getmaxyx()

    display_main_layout(y, x)
    thread.start_new_thread(display_ping_check, ())
    thread.start_new_thread(display_token_log, ())

    input_loop()

stdscr = curses.initscr()
main(stdscr)

if ex is not None:
    print 'got %s (%s)' % (type(ex).__name__, ex)
