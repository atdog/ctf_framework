#!/usr/bin/env bash

systemctl stop lxcfs
systemctl disable lxcfs

apt-get -y install cgroup-bin

cp /root/cgconfig.conf /etc/cgconfig.conf
cp /root/cgrules.conf /etc/cgrules.conf

/usr/sbin/cgconfigparser -l /etc/cgconfig.conf

pkill cgrulesengd
/usr/sbin/cgrulesengd
