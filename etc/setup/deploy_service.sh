#!/usr/bin/env bash

cd `dirname $0`

service=$1

[[ ! -n $service ]] && exit 1
# remove ctf user
getent passwd $service > /dev/null 2>&1
if [[ $? -eq 0  ]]; then
    deluser $service
fi
useradd -G services -m $service

find /home/$service -type f -exec rm -f {} \;

chmod 700 /home/$service
chown root:root /home/$service

cp $service/xinetd.conf /etc/xinetd.d/$service
cp $service/binary /home/$service/binary

if [[ -f $service/setup.sh ]]; then
    chmod +x $service/setup.sh
    $service/setup.sh
fi

echo 'dummy' > /home/flags/$service
chown root:$service /home/flags/$service
chmod 440 /home/flags/$service
chmod 700 /home/ctf
