#!/usr/bin/env bash

rm -rf /home/flags

# remove ctf user
getent passwd ctf > /dev/null 2>&1
if [[ $? -eq 0 ]]; then
        pkill -u ctf
        pkill -U ctf
        rm -rf /home/ctf
        deluser ctf --remove-all-files 2>/dev/null
fi

for service in `awk -F':' '/^services/{print $4}' /etc/group | sed 's/,/\n/g'`; do
    pkill -u $service
    pkill -U $service
    rm -rf /home/$service
    deluser $service --remove-all-files 2>/dev/null

    rm -f /etc/xinetd.d/$service
done
service xinetd restart

# delete group
getent group services > /dev/null 2>&1
if [[ $? -eq 0 ]]; then
        delgroup services
fi

rm -rf /root/*
