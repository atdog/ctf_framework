#!/usr/bin/env bash

cd `dirname $0`

service=$1

[[ ! -n $service ]] && exit 1

chown ctf:$service /home/$service
chown ctf:$service /home/$service/binary
chmod 750 /home/$service
chmod 750 /home/$service/binary
service xinetd restart
