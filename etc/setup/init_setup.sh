#!/usr/bin/env bash

#
# before executing this script, you have setup sshd config, ssh key, root password manually
#

# init ctf user
useradd -m ctf
chsh ctf -s /bin/bash
chmod 700 /home/ctf

# init service group
addgroup services

# setup flags folder
mkdir -p /home/flags
chown root:root /home/flags
chmod 755 /home/flags

# ASLR
echo 2 > /proc/sys/kernel/randomize_va_space
echo 2 > /proc/sys/kernel/yama/ptrace_scope

# put /etc/pam.d/su

# put /etc/sudoers
bin=/etc/sudoers
mv $bin{,.bak}
cp /root/sudoers $bin
chmod --reference ${bin}.bak $bin
chown --reference ${bin}.bak $bin
chgrp --reference ${bin}.bak $bin

# put /etc/security/limits.conf
bin=/etc/security/limits.conf
mv $bin{,.bak}
cp /root/limits.conf $bin
chmod --reference ${bin}.bak $bin
chown --reference ${bin}.bak $bin
chgrp --reference ${bin}.bak $bin

chmod +x /root/*.sh
# Control Groups
/root/cgroup.sh
