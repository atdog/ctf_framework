#!/usr/bin/env bash

cd `dirname $0`

service=$1

[[ ! -n $service ]] && exit 1

for s in `awk -F':' '/^services/{print $4}' /etc/group | sed 's/,/\n/g'`; do
    if [[ $s != $service ]]; then
        pkill -u $s
        pkill -U $s
        rm -rf /home/$s
        deluser $s --remove-all-files 2>/dev/null
        /root/scripts/cmd_${s}.py clean
    fi
done

getent passwd $service >/dev/null
if [[ $? -ne 0 ]]; then
    useradd -G services -m $service
fi

chmod -R 750 /home/$service
chown -R ctf:$service /home/$service

rm -f /home/flags/*
echo 'dummy' > /home/flags/$service
chown root:www-data /home/flags/$service
chmod 440 /home/flags/$service
chmod 700 /home/ctf
