#!/usr/bin/env python

from libs.initialize import *
from libs.gamebox import *
import subprocess
import types
import re
import os
from threading import Lock
import thread
from multiprocessing.pool import ThreadPool as Pool
import curses
import time
import requests
import json

teams = {}
host = '10.10.10.1:80'

def load_team():
    global host
    global teams

    m = api('get', 'http://%s/api/teams/1' % host)
    for team in json.loads(m):
        m = re.match(r'.*\.(?P<id>\d+)\.\d+\/\d+', team['ip'], re.DOTALL)
        if m == None:
            continue
        teams[m.group('id')] = team['name'].encode('utf-8')

    with open(CONFIG_ETC_PATH + '/teams.lst', 'w') as fh:
        fh.write(json.dumps(teams))

def api(method, url, data = {}, files = {}, timeout = 5):
    if method != 'get' and method != 'post':
        return 'Method %s not exitst' % (method)

    if len(data) > 0 or len(files) > 0:
        if method != 'post':
            return 'Method must be post'

    m = getattr(requests, method)

    r = None
    try:
        r = m(url, data = data, files = files, timeout = timeout)
    except requests.exceptions.Timeout:
        return 'API URL: %s timeout' % (url)
    except Exception as e:
        return 'API URL: %s fail, %s' % (url, str(e))

    if r.status_code != 200:
        return 'API URL: %s probably failed (%d)' % (url, r.status_code)

    return r.text.encode('utf-8')

def my_help(*args):
    """ display your help msg """
    import sys
    current_module = sys.modules[__name__]

    func_list = "Command List:\n\n"

    for a in dir(current_module):
        if isinstance(current_module.__dict__.get(a), types.FunctionType):
            if a.startswith('my_'):
                func = getattr(current_module, a)
                func_list += re.sub(r'^my_', '', a) + ": \n"
                func_list += "    " + str(func.func_doc) + "\n"

    return func_list

i = 0
def my_all(*args):
    """ parallel execute your command to all gambox
        all [pwn|web|all] cmd args ...  """
    if len(args[0]) < 3:
        return 'all type cmd args ...'

    output_win = args[0][0]
    target_type = args[0][1]
    cmd = args[0][2]
    cmdargs = []

    if len(args[0]) > 3:
        cmdargs = cmdargs + args[0][3:]

    if target_type != 'pwn' and target_type != 'web' and target_type != 'all':
        return 'please specify the type [pwn|web|all] for your target'

    import sys
    current_module = sys.modules[__name__]

    func = None
    try:
        func = getattr(current_module, 'my_' + cmd)
    except:
        return 'Command %s does not exist' % (cmd)

    if not func.__dict__.has_key('parallel') or func.__dict__['parallel'] != 1:
        return 'Function %s does not support multi-thread' % (cmd)

    (y, x) = output_win.getyx()
    output_win.move(y + 1, x)

    pool = Pool(8)

    if target_type == 'pwn' or target_type == 'all':
        for ip in PWN_GB_IP_LIST:
            # safe issurance
            newargs = cmdargs + [ip]
            pool.apply_async(parallel_handler, (func, newargs, output_win))

    if target_type == 'web' or target_type == 'all':
        for ip in WEB_GB_IP_LIST:
            # safe issurance
            newargs = cmdargs + [ip]
            pool.apply_async(parallel_handler, (func, newargs, output_win))

    pool.close()
    pool.join()

    (y, x) = output_win.getyx()
    output_win.addstr(y + 2, 1, '------------------ finish ---------------------', curses.color_pair(244))

    return ''

lock = Lock()
def parallel_handler(func, args, output_win):
    global lock

    args = [output_win] + args
    status = func(args)

    msg = 'IP: %10s, Status: %4s' % (args[-1], status)

    lock.acquire()

    (y, x) = output_win.getyx()
    if 'success' in status:
        output_win.addstr(y + 1, 1, msg, curses.color_pair(118))
    else:
        output_win.addstr(y + 1, 1, msg, curses.color_pair(10))

    output_win.refresh()

    lock.release()

def my_shell(*args):
    """ execute command in your local shell """
    if len(args[0]) <= 1:
        return 'shell your_cmd'
    try:
        p = subprocess.Popen(args[0][1:], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.wait()
        if p.returncode != 0:
            raise Exception("shell", "return code is not zero")
        return p.stdout.read()
    except:
        raise

def my_setup(*args):
    """ initial setup your gamebox """

    if len(args[0]) <= 1:
        return '[!] setup ip_address'

    target = args[0][1]

    try:
        gb = Gamebox(target)
        gb.init_setup()
        return 'Setup %s success' % target
    except:
        return 'Setup %s fail' % target

def my_reset_ctf(*args):
    """ reset ctf envrionment """
    global host
    output_win = args[0][0]

    # cleanup active service
    with open(CONFIG_ETC_PATH + '/service_pwn.lst', 'w') as fh:
        fh.write('')
    # cleanup active service
    with open(CONFIG_ETC_PATH + '/service_web.lst', 'w') as fh:
        fh.write('')

    # clean all ctf setup
    my_all([output_win, 'pwn', 'clean'])

    # clean round
    with open(CONFIG_ETC_PATH + '/current_round', 'w') as fh:
        fh.write('-1')

    # reset round
    m = api('get', 'http://%s/admin/reset_round/1' % host)
    (y, x) = output_win.getyx()
    output_win.addstr(y + 1, 1, m)

    # reset token
    m = api('get', 'http://%s/admin/reset_token' % host)
    (y, x) = output_win.getyx()
    output_win.addstr(y + 1, 1, m)

    return 'Reset Success'

def my_init_ctf(*args):
    """ initialize ctf envrionment """
    global host
    output_win = args[0][0]

    # setup all pwn gamebox
    my_all([output_win, 'pwn', 'setup'])

    # init round with 0
    with open(CONFIG_ETC_PATH + '/current_round', 'w') as fh:
        fh.write('0')

    # set ip
    m = api('get', "http://%s/admin/set_ip/1" % host)
    (y, x) = output_win.getyx()
    output_win.addstr(y + 1, 1, m)
    output_win.refresh()

    time.sleep(2)

    # gen key
    (y, x) = output_win.getyx()
    output_win.addstr(y + 1, 1, 'Gen SSH key')
    output_win.refresh()
    gen_key(output_win)

    time.sleep(2)

    # upload key
    (y, x) = output_win.getyx()
    output_win.addstr(y + 1, 1, 'Upload SSH key')
    output_win.refresh()
    upload_key_to_web(output_win)
    # load team information
    load_team()
    return 'Setup Success'

def my_deploy_service(*args):
    """ deploy your service to gamebox """
    # add into active service
    if len(args[0]) != 3:
        return '[!] deploy_service name ip_address'

    service = args[0][1]
    target = args[0][2]

    try:
        gb = Gamebox(target)
        gb.deploy_service(service)
        return 'Deploy %s on %s success' % (service, target)
    except:
        return 'Deploy %s on %s fail' % (service, target)

def my_start_service(*args):
    """ start to provide service """
    if len(args[0]) != 3:
        return '[!] start_service name ip_address'

    target = args[0][2]
    service = args[0][1]

    try:
        gb = Gamebox(target)
        gb.start_service(service)
        # run custom script
        return 'Start %s on %s success' % (service, target)
    except:
        return 'Start %s on %s fail' % (service, target)

def my_add_service_to_web(*args):
    """ add service to web manually"""
    global host
    if len(args[0]) != 3:
        return '[!] add_service_to_web type name'

    output_win = args[0][0]
    kind = args[0][1]
    service = args[0][2]

    tgt_lst = None

    if kind == 'pwn':
        tgt_lst = CONFIG_ETC_PATH + '/service_pwn.lst'
    # add into active service list
    elif kind == 'web':
        tgt_lst = CONFIG_ETC_PATH + '/service_web.lst'
    else:
        return '[!] kind must be pwn or web'

    found = False

    with open(tgt_lst, 'r') as fh:
        for line in fh:
            line = line.strip()
            if line == service:
                found = True

    if not found:
        if kind == 'pwn':
            with open(tgt_lst, 'a') as fh:
                fh.write(service + '\n')
        elif kind == 'web':
            with open(tgt_lst, 'w') as fh:
                fh.write(service + '\n')

    # update to web
    data = {
            'ctfid': 1,
            'name': service,
            'port': 0,
            'script': 'null'
            }
    if kind == 'web':
        data['bonus'] = 1

    m = api('post', 'http://%s/admin/add_service' % host, data=data)

    (y, x) = output_win.getyx()
    output_win.addstr(y + 1, 1, m)

    return ''

def my_clean(*args):
    """ clean up your gamebox """
    if len(args[0]) <= 1:
        return '[!] clean ip_address'

    target = args[0][1]

    try:
        gb = Gamebox(target)
        gb.clean()
        return 'Clean %s success' % target
    except:
        return 'Clean %s fail' % target

def my_gb_execute(*args):
    """ execute command on your gamebox """
    if len(args[0]) <= 2:
        return '[!] gb_execute cmd ip_address'

    output_win = args[0][0]
    cmd = args[0][1:-1]
    target = args[0][-1]

    try:
        gb = Gamebox(target)
        msg = gb.execute_on_gamebox(' '.join(cmd))

        out = msg.split('\n')
        return 'Execute on %10s success: %s' % (target, out[0])
    except:
        return 'Execute on %10s fail' % target

ping_status = {}
ping_check_running = False

def ping_check_thread(gb):
    global ping_status
    global ping_lock

    try:
        v = gb.ping_check()
        ping_status[gb.ip] = v
    except:
        ping_status[gb.ip] = -1

def ping_check_handler():
    while True:
        pool = Pool(8)

        for ip in PWN_GB_IP_LIST:
            gb = Gamebox(ip)
            pool.apply_async(ping_check_thread, (gb, ))

        for ip in WEB_GB_IP_LIST:
            gb = Gamebox(ip)
            pool.apply_async(ping_check_thread, (gb, ))

        pool.close()
        pool.join()

        time.sleep(5)

def ping_check_background():
    global ping_check_running
    if not ping_check_running:
        thread.start_new_thread(ping_check_handler, ())
        ping_check_running = True

def my_ping_check(*args):
    """ ping check your gamebox """
    if len(args[0]) <= 1:
        return '[!] ping_check ip_address'

    ping_check_background()

    target = args[0][1]

    global ping_status
    if ping_status.has_key(target):
        return 'Ping on %s success: %.3f [cache]' % (target, ping_status[target])

    try:
        gb = Gamebox(target)
        v = gb.ping_check()
        return 'Ping on %s success: %.3f' % (target, v)
    except:
        return 'Ping on %s fail' % (target)

    return ''

def gen_key(output_win):
    global host
    m = api('get', 'http://%s/api/team_ips/1' % host)
    m = json.loads(m)
    for entry in m:
        ##########################
        net = re.sub(r'\.\d+\/\d+$', '', entry['ip'])
        pwn = net + '.1'
        web = net + '.2'
        ##########################
        # generate key for each team
        key_path = CONFIG_KEY_DIR_PATH + '/gamebox/ctf@%s.key' % (net)
        if os.path.exists(key_path):
            os.unlink(key_path)
        cmd = 'ssh-keygen -t rsa -b 4096 -N "" -f %s' % (key_path)
        cmd = shlex.split(cmd)
        my_shell([None] + cmd)
        ##########################
        # copy to gamebox
        # set proper permission
        try:
            gb = Gamebox(pwn)
            gb.upload_key_to_gb(key_path + '.pub')
        except Exception as e:
            msg ='faile - ' + str(e)
            (y, x) = output_win.getyx()
            output_win.addstr(y + 1, 1, msg)

        try:
            gb = Gamebox(web)
            gb.upload_key_to_gb(key_path + '.pub')
        except Exception as e:
            msg ='faile - ' + str(e)
            (y, x) = output_win.getyx()
            output_win.addstr(y + 1, 1, msg)
        output_win.refresh()

    return ''

def upload_key_to_web(output_win):
    global host
    m = api('get', 'http://%s/api/team_ips/1' % host)
    m = json.loads(m)
    for entry in m:
        ##########################
        net = re.sub(r'\.\d+\/\d+$', '', entry['ip'])
        ip = net + '.1'
        key_path = CONFIG_KEY_DIR_PATH + '/gamebox/ctf@%s.key' % (net)

        files = { 'private_key': open(key_path, 'r') }
        data = { 'ip': ip }
        # gamebox key
        m = api('post', 'http://%s/admin/upload_private_key' % host, files=files, data=data)
        m = m.strip()
        (y, x) = output_win.getyx()
        output_win.addstr(y + 1, 1, m)
        output_win.refresh()
    return ''

def my_copy_to_gb(*args):
    """ copy file to gamebox """
    if len(args[0]) <= 3:
        return '[!] copy_to_gb src_file tgt_file ip_address'

    src = args[0][1]
    tgt = args[0][2]
    target = args[0][3]

    try:
        gb = Gamebox(target)
        v = gb.copy_from_local(src, tgt)
        return 'copy to %s success' % (target)
    except:
        return 'copy to %s fail' % (target)

    return ''

def my_change_token(*args):
    """ change token and update through api """
    if len(args[0]) <= 2:
        return '[!] change_token service ip_address'

    from change_token import gen_token, update_web

    output_win = args[0][0]
    service = args[0][1]
    target = args[0][2]

    try:
        token = gen_token()
        (y, x) = output_win.getyx()
        output_win.addstr(y + 2, 1, "New Token for service %s: %s" % (service, token))
        output_win.refresh()

        gb = Gamebox(target)
        gb.change_token(service, token)
    except Exception as e:
        return 'update to gb failed' % (str(e))


    m = None
    try:
        m = update_web(target, service, token)
    except Exception as e:
        return 'update to api failed - %s' % (str(e))

    return 'update success - %s' % (m)

def my_hide_web_service(*args):
    """ hide web service on scoreboard """
    if len(args[0]) <= 1:
        return '[!] hide_web_service service'

    output_win = args[0][0]
    service = args[0][1]

    data = {
            'name': service
            }
    m = None
    try:
        m = api('post', 'http://%s/admin/close_service' % host, data=data)
    except Exception as e:
        return 'hide service fail - %s' % (str(e))

    return m

def my_show_web_service(*args):
    """ show web service on scoreboard """
    if len(args[0]) <= 1:
        return '[!] show_web_service service'

    output_win = args[0][0]
    service = args[0][1]

    data = {
            'name': service
            }
    m = None
    try:
        m = api('post', 'http://%s/admin/open_service' % host, data=data)
    except Exception as e:
        return 'show service fail - %s' % (str(e))

    return m

def my_init_web_service(*args):
    """ init web service on web gamebox """
    if len(args[0]) <= 2:
        return '[!] init_web_service service ip_address'

    output_win = args[0][0]
    service = args[0][1]
    target = args[0][2]

    try:
        gb = Gamebox(target)
        gb.execute_on_gamebox('/root/init_web_service.sh %s' % (service))
    except Exception as e:
        return 'init service failed' % (str(e))

    return 'init success'

my_ping_check.parallel = 1
my_setup.parallel = 1
my_deploy_service.parallel = 1
my_start_service.parallel = 1
my_clean.parallel = 1
my_gb_execute.parallel = 1
my_copy_to_gb.parallel = 1
my_init_web_service.parallel = 1

if __name__ == '__main__':
    load_team()
    print 'main'
